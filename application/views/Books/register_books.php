<?php
	if (isset($msg)) {
		echo "<div class=\"alert alert-success\" role=\"alert\">".$msg."</div>";
	}
	if (isset($error)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>";
	}
?>
<div class="container">
<!-- Se agrega el metodo para mandar los datos -->
<form action="<?= base_url('Home/SaveBook') ?>" method="POST" enctype="multipart/form-data">
	<!-- Titulo de Datos del Usuario -->
	<h3>Datos del Libro</h3>
	<p class="text-right">Datos que son Requeridos (*)</p>
	<hr>
	<!-- formamos un grupo de elementos -->
	<div class="form-group">
		<!-- Fila de Datos de Usuario -->
    <div class="form-row">
			<!-- Atributos para el Nombre de Usuarios -->
      <div class="col-5">
        <label for="">Titulo*</label>
        <input type="text" name="title" class="form-control" placeholder="Titulo" value="<?= set_value('title') ?>">
				<div class="text-danger"><?= form_error('title') ?></div>
			</div>
			<!-- Atributos para el Correo -->
      <div class="col">
      	<label for="">Autor(es)*</label>
        <input type="text" name="autor" class="form-control" placeholder="Autor" value="<?= set_value('autor') ?>">
				<div class="text-danger"><?= form_error('autor') ?></div>
			</div>
			<div class="col">
      	<label for="">Editorial*</label>
        <input type="text" name="editorial" class="form-control" placeholder="Editorial" value="<?= set_value('editorial') ?>">
				<div class="text-danger"><?= form_error('editorial') ?></div>
			</div>
    </div>
	  <br>
	<!-- formamos un grupo de elementos -->
  <div class="form-group">
		<!-- Fila de Datos de la Informacion Basica del Usuario -->
		<div class="form-row">
			<!-- Atributos para el Nombre o Nombres del Usuario -->
			<div class="col-5">
				<label for="">Formato*</label>
				<input name="format" class="form-control" type="text" placeholder="Formato" value="<?= set_value('format') ?>">
				<div class="text-danger"><?= form_error('format') ?></div>
			</div>
			<!-- Atributos para los Apellidos del Usuario -->
			<div class="col-5">
				<label for="">Precio*</label>
				<input name="price" class="form-control" type="text" placeholder="Precio" value="<?= set_value('price') ?>">
				<div class="text-danger"><?= form_error('price') ?></div>
			</div>
			<!-- Atributos para la Edad del Usuario -->
			<div class="col">
				<label for="">Año*</label>
				<input name="year" type="text" class="form-control" placeholder="Año" value="<?= set_value('year') ?>">
				<div class="text-danger"><?= form_error('year') ?></div>
			</div>
		</div>
		<!-- Fila de Datos de la Informacion de Localizacion del Usuario -->
    <div class="form-row">
			<!-- Atributos para la dirreccion del Usuario -->
      <div class="col-4">
				<label for="">ISBN*</label>
				<input name="isbn" class="form-control" type="text" placeholder="ISBN" value="<?= set_value('isbn') ?>">
				<div class="text-danger"><?= form_error('isbn') ?></div>
			</div>
			<div class="col">
				<label for="">Categoría*</label>
        <select name="cat" class="custom-select">
        	<option selected value="">Seleccione Categoría</option>
					<?php foreach($category as $item): ?>
						<option <?= set_value('cat') == $item->Name ? 'selected' : ''; ?> value="<?= $item->IdCategory ?>"><?= $item->Name ?></option>
					<?php endforeach; ?>
      	</select>
				<div class="text-danger"><?= form_error('cat') ?></div>
			</div>
      <div class="col">
        <label for="image">Direccion de imagen</label>
				<input type="file" id="image" name="image" disabled>
        <div class="text-danger"><?= form_error('image') ?></div>
      </div>
    </div>
		<div class="form-row">
			<!-- Atributos para el telefono del Usuario -->
      <div class="col-8">
				<label for="">Descripción*</label>
        <textarea name="desc"  class="form-control" rows="5" placeholder="Descripción" value="<?= set_value('desc') ?>"></textarea>
				<div class="text-danger"><?= form_error('desc') ?></div>
			</div>
		</div>
	</div>
	<!-- Boton para el Agregar Todos los Datos -->
  <div class="form-group">
    <input type="submit" class="btn btn-info" value="Registrar">
  </div>
</form>
</div>