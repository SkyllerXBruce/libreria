<!-- Formulario del Dashboard -->
<!-- Agregamos el Encabezado -->
<?= $head ?>
<!-- Agregamos el Nav -->
<?= $nav ?>
<!-- Contenemos los Siguentes Componentes -->
<div class="container-fluid">
    <div class="row">
        <!-- Agregamos el Contenido del Dashboard -->
        <main class="col-mx-12 mx-auto pt-3 px-2">
            <?= $content ?>
        </main>
    </div>
</div>
<!-- Agregamos el Footer -->
<?= $footer ?>