<?php
	if (isset($msg)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">".$msg."</div>";
	}
?>
<!-- Se agrega el metodo para mandar los datos -->
<form action="<?= base_url('Home/registerUser') ?>" method="POST">
	<!-- Titulo de Datos del Usuario -->
	<h3>Datos del Usuario</h3>
	<p class="text-right">Datos que son Requeridos (*)</p>
	<hr>
	<!-- formamos un grupo de elementos -->
	<div class="form-group">
		<!-- Fila de Datos de Usuario -->
    <div class="form-row">
			<!-- Atributos para el Nombre de Usuarios -->
      <div class="col-5">
        <label for="">Nombre Usuario*</label>
        <input type="text" name="user" class="form-control" placeholder="Inserte nombre de usuario" value="<?= set_value('user') ?>">
				<div class="text-danger"><?= form_error('user') ?></div>
			</div>
			<!-- Atributos para el Correo -->
      <div class="col">
      	<label for="">Correo*</label>
        <input type="text" name="correo" class="form-control" placeholder="correo@mail.com" value="<?= set_value('correo') ?>">
				<div class="text-danger"><?= form_error('correo') ?></div>
			</div>
			<div class="col">
      	<label for="">Contraseña*</label>
        <input type="password" name="password" class="form-control" placeholder="Contraseña" value="<?= set_value('password') ?>">
				<div class="text-danger"><?= form_error('password') ?></div>
			</div>
    </div>
	<br>
	<!-- Titulo de Datos de la Informacion del Usuario -->
  <h3>Información del Usuario</h3>
	<hr>
	<!-- formamos un grupo de elementos -->
  <div class="form-group">
		<!-- Fila de Datos de la Informacion Basica del Usuario -->
		<div class="form-row">
			<!-- Atributos para el Nombre o Nombres del Usuario -->
			<div class="col-5">
				<label for="">Nombre(s)*</label>
				<input name="name" class="form-control" type="text" placeholder="Inserte su nombre" value="<?= set_value('name') ?>">
				<div class="text-danger"><?= form_error('name') ?></div>
			</div>
			<!-- Atributos para los Apellidos del Usuario -->
			<div class="col-5">
				<label for="">Apellidos*</label>
				<input name="lastname" class="form-control" type="text" placeholder="Inserte sus apellidos" value="<?= set_value('lastname') ?>">
				<div class="text-danger"><?= form_error('lastname') ?></div>
			</div>
			<!-- Atributos para la Edad del Usuario -->
			<div class="col">
				<label for="">Edad*</label>
				<input name="edad" type="text" class="form-control" placeholder="Edad" value="<?= set_value('edad') ?>">
				<div class="text-danger"><?= form_error('edad') ?></div>
			</div>
		</div>
		<!-- Fila de Datos de la Informacion de Localizacion del Usuario -->
    <div class="form-row">
			<!-- Atributos para la dirreccion del Usuario -->
			<div class="col-8">
				<label for="">Dirección*</label>
				<input name="dir" class="form-control" type="text" placeholder="Inserte su Dirección(calle, #)" value="<?= set_value('dir') ?>">
				<div class="text-danger"><?= form_error('dir') ?></div>
			</div>
			<div class="col">
				<label for="">Alcaldia*</label>
				<input name="alcaldia" class="form-control" type="text" placeholder="Inserte Alcaldia" value="<?= set_value('alcaldia') ?>">
				<div class="text-danger"><?= form_error('alcaldia') ?></div>
			</div>
    </div>
		<div class="form-row">
			<!-- Atributos para el telefono del Usuario -->
			<div class="col-4">
				<label for="">CP*</label>
				<input name="cp" class="form-control" type="text" placeholder="Inserte CodigoPostal" value="<?= set_value('cp') ?>">
				<div class="text-danger"><?= form_error('cp') ?></div>
			</div>
			<div class="col">
				<label for="">Teléfono Local / celular</label>
				<input name="tel" type="text" class="form-control" placeholder="Ingrese su Telefono" value="<?= set_value('tel') ?>">
				<div class="text-danger"><?= form_error('tel') ?></div>
			</div>
		</div>
	</div>
	<!-- Boton para el Agregar Todos los Datos -->
  <div class="form-group">
    <input type="submit" class="btn btn-info" value="Registrar">
  </div>
</form>