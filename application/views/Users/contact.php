<div class="container">
  <div class="row">
    <div class="col">
      <div class="card bg-light">
        <div class="card-header bg-info text-white">Contáctanos</div>
        <div class="card-body">
          <form>
            <div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
              <label>Correo</label>
              <input type="email" class="form-control" placeholder="Ingresa Correo">
              <small id="emailHelp" class="form-text text-muted">Protegemos tus Datos, esta información no será compartida con nadie.</small>
            </div>
            <div class="form-group">
            	<label for="message">Mensaje</label>
              <textarea class="form-control" rows="6"></textarea>
            </div>
            <div class="mx-auto">
							<button type="submit" class="btn btn-outline-secondary text-right">Enviar</button>
						</div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-6">
      <div class="card bg-light mb-3">
        <div class="card-header bg-info text-white">Encuentranos en:</div>
        <div class="card-body">
          <p>Avenida San Rafael Atlixco 186</p>
          <p>Colonia Vicentina</p>
          <p>CP. 09340, Alcaldia Iztapalapa</p>
          <p>Email : nuevoportaluami@xanum.uam.mx</p>
          <p>Tel. 5804-4600</p>
				</div>
      </div>
		</div>
	</div>
  <br>
</div>