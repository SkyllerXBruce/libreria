<section class="section-content bg padding-y-sm">
  <div class="container">
    <div class="row">
      <div class="col-xl-10 col-md-9 col-sm-12">
        <h1>¿Ya te vas?...</h1>
        <h2>Ve nuestra recomendación antes de irte</h2>
        <main class="card">
	        <div class="row no-gutters">
		        <aside class="col-sm-6 border-right">
              <article class="gallery-wrap"> 
                <div class="img-big-wrap">
                  <div><img src="<?= base_url($book[0]->DirImage)?>"></div>
                </div> <!-- slider-product.// -->
              </article> <!-- gallery-wrap .end// -->
		        </aside>
		        <aside class="col-sm-6">
              <article class="card-body">
                <!-- short-info-wrap -->
                <h3 class="title mb-3"><?= $book[0]->Title ?></h3>
                <div class="mb-3"> 
                  <var class="price h3 text-warning"> 
                    <span class="currency">MX $</span><span class="num"><?= $book[0]->Price ?></span>
                  </var> 
                </div> <!-- price-detail-wrap .// -->
                <dl>
                  <dt>Descripción</dt>
                  <dd><p><?= $book[0]->Description ?></p></dd>
                </dl>
                <dl class="row">
                  <dt class="col-sm-3">Autor</dt>
                  <dd class="col-sm-9"><?= $book[0]->Author ?></dd>

                  <dt class="col-sm-3">Editorial</dt>
                  <dd class="col-sm-9"><?= $book[0]->Editorial ?></dd>

                  <dt class="col-sm-3">Año</dt>
                  <dd class="col-sm-9"><?= $book[0]->Year ?></dd>
                </dl>
                <hr>
	              <div class="row">
                  <div class="col-sm-7">
                    <dl class="dlist-inline">
                        <dt>ISBN: </dt>
                        <dd><?= $book[0]->ISBN ?></dd>
                    </dl>  <!-- item-property .// -->
                  </div> <!-- col.// -->
	              </div> <!-- row.// -->
                <hr>
                <form action="<?= base_url('Buying/buy') ?>" method="POST">
                  <input type="hidden" name="IdBook" value="<?= $book[0]->IdBook ?>">
                  <button class="btn btn-outline-success" type="submit">Comprar</button>
                </form>
                <!-- short-info-wrap .// -->
              </article> <!-- card-body.// -->
            </aside> <!-- col.// -->
          </div> <!-- row.// -->
        </main> <!-- card.// -->
        <!-- PRODUCT DETAIL .// -->
      </div> <!-- col // -->
      <aside class="col-xl-2 col-md-3 col-sm-12">
        <h4>Información de la Cuenta</h4>
        <div class="card">
          <div class="card-body small">
            <!--<span>¿Cambiar alguna información de la Cuenta?</span> 
            <hr><a href="< ?= base_url('Home/modifyUser') ?>">Modificar Cuenta</a><br> 
            <hr>-->
            <span>¿Desea Salir?</span> 
            <hr><a href="<?= base_url('Home/exit') ?>">Salir de la Cuenta</a><br> 
          </div> <!-- card-body.// -->
        </div> <!-- card.// -->
      </aside> <!-- col // -->
    </div> <!-- row.// -->
  </div><!-- container // -->
</section>
<br>
