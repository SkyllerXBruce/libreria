<!-- Aside -->
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	<!-- Caracteristicas de la Barra -->
	<style>
		.sidebar-sticky{
			position: -webkit-sticky;
			position: sticky;
			top: 78px;
			height: calc(100vh - 78px);
			padding-top: .5rem;
			overflow-x: hidden;
			overflow-y: auto;
		}
	</style>
	<div class="sidebar-sticky" style="margin-top: 1em;">
		<!-- Links de la Barra Lateral -->
		<div class="nav nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
			<a class="navbar-brand text-secondary" href="<?=base_url('Home/category')?>">
				<img src="<?=base_url('assets/images/books.png')?>" width="25" height="25" alt="contact"> Libros
			</a>
			<?php
				if($category!=FALSE && $subcategory!=FALSE){
					for ($i=0; $i < count($category) ; $i++) { 
						if(!empty($subcategory[$i])){
							echo "<div class=\"btn-group dropright\">";
								echo "<a class=\"btn dropdown-toggle\" data-toggle=\"dropdown\">".$category[$i]."</a>";
								echo "<div class=\"dropdown-menu\">";
									for ($j=0; $j < count($subcategory[$i]); $j++) {
										echo "<a class=\"dropdown-item\" href=".base_url('Home/searchCategory/').$subcategory[$i][$j].">".$subcategory[$i][$j]."</a>";
									}
								echo "</div>";
							echo "</div>";
						}else {
							echo "<a class=\"btn\" href=".base_url('Home/searchCategory/').$category[$i].">".$category[$i]."</a>";
						}
					}
				}
			?>
		</div>
	</div>
</nav>