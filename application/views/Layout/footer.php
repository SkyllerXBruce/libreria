<!-- Footer -->
<footer class="page-footer font-small bg-secondary pt-4">
  <!-- Footer Elements -->
  <div class="container">
		<div class="row">
			<ul class="text-center"><li><small>Autores:</small></li></ul>
			<div class="col-3">
				<ul class="list-style:none">
					<li><small>Avilés Martínez Abraham</small></li>
					<li><small>Cruz González Jorge Luis</small></li>
				</u>
			</div>
			<div class="col-3">
				<ul class="list-style:none">
					<li><small>Mercado Velasco Miguel Angel</small></li>
					<li><small>Rodríguez Pérez Blanca Esthela</small></li>
				</ul>
			</div>
			<div class="col-3">
				<ul class="text-center">
					<?php 
						if(!$is_online){ 
							echo "<li class=\"list-inline-item\">";
								echo "<h5 class=\"mb-1 text-dark\">¿No se ha registrado? </h5>";
								echo "<a href=".base_url('Home/register')." class=\"btn btn-dark\">¡Registrese!</a>";
							echo "</li>";
						}else{ 
							if($is_admin){
								echo "<li class=\"list-inline-item\">";
									echo "<a class=\"nav-link  text-dark\" href=".base_url('Home/RegisterBook')."><strong>Agregar un Libro</strong></a>";
								echo "</li>";
							} 
						}
					?>
				</ul>
			</div>
		</div>
  </div>
  <!-- Footer Elements -->
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <label>Venta de Libros TSBD</label>
  </div>
</footer>

		<!-- Pie de Pagina del Formulario Dashboard -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
		<script src="<?= base_url('assets/js/auth/login.js') ?>"></script>
  </body>
</html>