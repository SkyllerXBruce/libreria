<?php
	if (isset($error)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">".$error."</div>";
	}
?>
<section class="section-content bg padding-y">
	<?php if(!empty($book)): ?>
		<div class="container">
			<div class="row">
				<main class="col-sm-9">
					<div class="card">
						<table class="table table-hover shopping-cart-wrap">
							<thead class="text-muted">
								<tr>
									<th scope="col">Libro</th>
									<th scope="col" width="120">Precio</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<figure class="media">
											<div class="col	">
												<img src="<?= base_url($book[0]->DirImage) ?>" width="150" height="200">
												<p><h6 class="title"><?= $book[0]->Title ?></h6></p>
											</div>
											<figcaption class="media-body">
												<dl class="dlist-inline small">
													<dt>Autor:</dt>
													<dd><?= $book[0]->Author ?></dd>
												</dl>
												<dl class="dlist-inline small">
													<dt>Editorial: </dt>
													<dd><?= $book[0]->Editorial ?></dd>
												</dl>
											</figcaption>
										</figure> 
									</td>
									<td> 
										<div class="price-wrap"> 
											<var class="price">$ <?= $book[0]->Price ?></var> 
											<small class="text-muted">(Pesos MX)</small>
											<hr>
											<a href="<?= base_url('Home') ?>" class="btn btn-outline-danger">Cancelar Compra</a>
										</div> <!-- price-wrap .// -->
									</td>
								</tr>
							</tbody>
						</table>
					</div> <!-- card.// -->
				</main> <!-- col.// -->
				<aside class="col-sm-3">
					<dl class="dlist-align">
						<dt>Precio Total: </dt>
						<dd class="text-right">$ <?= $book[0]->Price ?></dd>
					</dl>
					<dl class="dlist-align">
						<dt>Envio:</dt>
						<dd class="text-right">$ <?= $pricesend ?></dd>
					</dl>
					<dl class="dlist-align h4">
						<?php $res=($book[0]->Price+$pricesend)*1.16 ?>
						<dt>Total: <p><small>(+ iva)</small></p> </dt>
						<dd class="text-right"><strong><?= $res ?></strong></dd>
					</dl>
					<hr>
					<figure class="itemside mb-3">
						<form action="<?= base_url('Buying/FinishBuy') ?>" method="POST">
							<input type="hidden" name="IdBook" value="<?= $book[0]->IdBook ?>">
							<input type="hidden" name="Total" value="<?= $res ?>">
							<input type="hidden" name="Type" value="<?= $type ?>">
							<button type="submit" class="btn btn-outline-success">Finalizar Compra</button>
						</form>
						<hr>
						<a href="<?= base_url('Buying/ChangeBuyPay/').$book[0]->IdBook ?>" class="btn btn-outline-info">Cambiar Método de Pago</a>
					</figure>
				</aside> <!-- col.// -->
			</div>
		</div> <!-- container .//  -->
	<?php endif; ?>
</section>
<br>