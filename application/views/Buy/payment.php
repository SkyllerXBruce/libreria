<?php
	if (isset($msg)) {
		echo "<div class=\"alert alert-danger\" role=\"alert\">".$msg."</div>";
	}
?>
<article class="card">
 <div class="card-body p-5">
		<ul class="nav bg radius nav-pills nav-fill mb-3" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="pill" href="#nav-tab-card">tarjeta de Crédito/Debito</a></li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#nav-tab-paypal">Efectivo a la Entrega</a></li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#nav-tab-bank">Transferencia bancaria</a></li>
		</ul>
			<div class="tab-content">
			 	<div class="tab-pane fade show active" id="nav-tab-card">
				<form action="<?= base_url('Buying/buyCompleted') ?>" method="POST">
					<input type="hidden" name="IdBook" value="<?= $idbook ?>">
					<div class="form-group">
						<label>Nombre Completo (de la tarjeta)</label>
						<input type="text" class="form-control" name="namecard" placeholder="Nombre de Tarjeta" value="<?= set_value('namecard') ?>">
						<div class="text-danger"><p><?= form_error('namecard') ?></p></div>
					</div> <!-- form-group.// -->
					<div class="form-group">
						<label>Número de la Tarjeta</label>
						<div class="input-group">
							<input type="text" class="form-control" name="numbercard" placeholder="Número de la Tarjeta" value="<?= set_value('numbercard') ?>">
							<div class="input-group-append">
								<span class="input-group-text text-muted">
									<img class="logo" src="<?= base_url('assets/images/pay-visa.png') ?>" width="28" height="16">
									<img class="logo" src="<?= base_url('assets/images/pay-american-ex.png') ?>" width="28" height="16">
									<img class="logo" src="<?= base_url('assets/images/pay-mastercard.png') ?>" width="28" height="16">
								</span>
							</div>
						</div>
						<div class="text-danger"><p><?= form_error('numbercard') ?></p></div>
					</div> <!-- form-group.// -->
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								<label><span class="hidden-xs">Expira</span> </label>
								<div class="input-group">
									<select name="month" class="custom-select">
										<option selected value="">Mes</option>
										<option <?= set_value('month') == 'Enero' ? 'selected' : ''; ?> value="Enero">Enero</option>
										<option <?= set_value('month') == 'Febrero' ? 'selected' : ''; ?> value="Febrero">Febrero</option>
										<option <?= set_value('month') == 'Marzo' ? 'selected' : ''; ?> value="Marzo">Marzo</option>
										<option <?= set_value('month') == 'Abril' ? 'selected' : ''; ?> value="Abril">Abril</option>
										<option <?= set_value('month') == 'Mayo' ? 'selected' : ''; ?> value="Mayo">Mayo</option>
										<option <?= set_value('month') == 'Junio' ? 'selected' : ''; ?> value="Junio">Junio</option>
										<option <?= set_value('month') == 'Julio' ? 'selected' : ''; ?> value="Julio">Julio</option>
										<option <?= set_value('month') == 'Agosto' ? 'selected' : ''; ?> value="Agosto">Agosto</option>
										<option <?= set_value('month') == 'Septiembre' ? 'selected' : ''; ?> value="Septiembre">Septiembre</option>
										<option <?= set_value('month') == 'Octubre' ? 'selected' : ''; ?> value="Octubre">Octubre</option>
										<option <?= set_value('month') == 'Noviembre' ? 'selected' : ''; ?> value="Noviembre">Noviembre</option>
										<option <?= set_value('month') == 'Diciembre' ? 'selected' : ''; ?> value="Diciembre">Diciembre</option>
									</select>
									<select name="year" class="custom-select">
										<option selected value="">Año</option>
										<option <?= set_value('year') == '2019' ? 'selected' : ''; ?> value="2019">2019</option>
										<option <?= set_value('year') == '2020' ? 'selected' : ''; ?> value="2020">2020</option>
										<option <?= set_value('year') == '2021' ? 'selected' : ''; ?> value="2021">2021</option>
										<option <?= set_value('year') == '2022' ? 'selected' : ''; ?> value="2022">2022</option>
										<option <?= set_value('year') == '2023' ? 'selected' : ''; ?> value="2023">2023</option>
										<option <?= set_value('year') == '2024' ? 'selected' : ''; ?> value="2024">2024</option>
										<option <?= set_value('year') == '2025' ? 'selected' : ''; ?> value="2025">2025</option>
										<option <?= set_value('year') == '2026' ? 'selected' : ''; ?> value="2026">2026</option>
										<option <?= set_value('year') == '2027' ? 'selected' : ''; ?> value="2027">2027</option>
										<option <?= set_value('year') == '2028' ? 'selected' : ''; ?> value="2028">2028</option>
										<option <?= set_value('year') == '2029' ? 'selected' : ''; ?> value="2029">2029</option>
										<option <?= set_value('year') == '2030' ? 'selected' : ''; ?> value="2030">2030</option>
										<option <?= set_value('year') == '2031' ? 'selected' : ''; ?> value="2031">2031</option>
										<option <?= set_value('year') == '2032' ? 'selected' : ''; ?> value="2032">2032</option>
										<option <?= set_value('year') == '2033' ? 'selected' : ''; ?> value="2033">2033</option>
										<option <?= set_value('year') == '2034' ? 'selected' : ''; ?> value="2034">2034</option>
										<option <?= set_value('year') == '2035' ? 'selected' : ''; ?> value="2035">2035</option>
										<option <?= set_value('year') == '2036' ? 'selected' : ''; ?> value="2036">2036</option>
										<option <?= set_value('year') == '2037' ? 'selected' : ''; ?> value="2037">2037</option>
										<option <?= set_value('year') == '2038' ? 'selected' : ''; ?> value="2038">2038</option>
										<option <?= set_value('year') == '2039' ? 'selected' : ''; ?> value="2039">2039</option>
									</select>
								</div>
								<div class="row">
									<div class="col"><div class="text-danger"><?= form_error('month') ?></div></div>
									<div class="col"><div class="text-danger"><?= form_error('year') ?></div></div>
								</div>
							</div>
						</div>
					</div> <!-- row.// -->
					<button class="btn btn-primary" type="submit">Confirmar</button>
				</div> <!-- tab-pane.// -->
				<div class="tab-pane fade" id="nav-tab-paypal">
					<p>Pago a la Entrega</p>
					<p><a href="<?= base_url('Buying/buyedNoCard/').$idbook.'/Cash' ?>" type="submit" class="btn btn-primary">Confirmar</a></p>
					<p><strong>Nota:</strong>Ud. Se hace responsable de el pago total de el/los productos adquiridos al momento de la entrega.</p>
					<p><small>Cualquier abuso se aplicará todo el peso de la ley</small></p>
				</div>
				<div class="tab-pane fade" id="nav-tab-bank">
					<p>Datos de la cuenta</p>
					<dl class="param">
						<dt>BANCO: </dt>
						<dd>Grupo Financiero Inbursa</dd>
					</dl>
					<dl class="param">
						<dt>Numero de Cuenta: </dt>
						<dd>12345678912345678</dd>
					</dl>
					<dl class="param">
						<dt>IBAN: </dt>
						<dd>MX789</dd>
					</dl>
					<p><a href="<?= base_url('Buying/buyedNoCard/').$idbook.'/Bank'?>" type="submit" class="btn btn-primary">Confirmar</a></p>
					<p><strong>Nota:</strong> Se notificará al usuario del pago autorizado y envió de su Compra</p>
					<p><small>En caso del pago no autorizado se cancelará su compra</small></p>
				</div> <!-- tab-pane.// -->
			</div> <!-- tab-content .// -->
		</form>
	</div> <!-- card-body.// -->
</article> <!-- card.// -->
<br>