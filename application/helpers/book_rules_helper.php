<?php
// Verifica si la Funcion del helper existe 
if(!function_exists('getCreateBookRules')){
    // Funcion que regresa las reglas que van a tener los datos ingresados del formulario Create Users
    function getCreateBookRules(){
        return array(
            array(
                'field' => 'title', // Nombre del Identificador 
                'label' => 'Titulo', // nombre de la etiqueta 
                'rules' => 'required|max_length[150]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'max_length' => 'El %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'autor', // Nombre del Identificador 
                'label' => 'Autor', // nombre de la etiqueta 
                'rules' => 'required|max_length[150]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'max_length' => 'El %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'editorial', // Nombre del Identificador 
                'label' => 'Editorial', // nombre de la etiqueta 
                'rules' => 'required|max_length[50]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'La %s es requerida.', // manda error si no hay datos encontrados
                    'max_length' => 'La %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'year',
                'label' => 'Año',
                'rules' => 'required|is_natural_no_zero|max_length[4]',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'is_natural_no_zero' => 'El %s no es Valido.', // manda error si el dato no es un numero natural
                    'max_length' => 'El %s sobre pasa la cantidad de digitos permitidos'
                )
            ),
            array(
                'field' => 'format', // Nombre del Identificador 
                'label' => 'Formato', // nombre de la etiqueta 
                'rules' => 'required|max_length[50]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'max_length' => 'El %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'isbn',
                'label' => 'ISBN',
                'rules' => 'required|is_natural_no_zero|max_length[4]',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'is_natural_no_zero' => 'El %s no es Valido.', // manda error si el dato no es un numero natural
                    'max_length' => 'El %s sobre pasa la cantidad de digitos permitidos'
                )
            ),
            array(
                'field' => 'cat', // Nombre del Identificador 
                'label' => 'Categoría', // nombre de la etiqueta 
                'rules' => 'required|max_length[50]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'La %s es requerida.', // manda error si no hay datos encontrados
                    'max_length' => 'La %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'price',
                'label' => 'Precio',
                'rules' => 'required|is_natural_no_zero|max_length[4]',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'is_natural_no_zero' => 'El %s no es Valido.', // manda error si el dato no es un numero natural
                    'max_length' => 'El %s sobre pasa la cantidad de digitos permitidos'
                )
            ),
            array(
                'field' => 'desc', // Nombre del Identificador 
                'label' => 'Descripción', // nombre de la etiqueta 
                'rules' => 'required|max_length[150]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'La %s es requerida.', // manda error si no hay datos encontrados
                    'max_length' => 'La %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
        );
    }
}