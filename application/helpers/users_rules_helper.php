<?php
// Verifica si la Funcion del helper existe 
if(!function_exists('getCreateUserRules')){
    // Funcion que regresa las reglas que van a tener los datos ingresados del formulario Create Users
    function getCreateUserRules(){
        return array(
            array(
                'field' => 'user', // Nombre del Identificador 
                'label' => 'Usuario', // nombre de la etiqueta 
                'rules' => 'required|max_length[100]', // reglas separadas por un pipe |
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'max_length' => 'El %s es demaciado grande' // // manda error si el campo usuario sobrepasa 100 caracteres
                )
            ),
            array(
                'field' => 'correo',
                'label' => 'Correo Electrónico',
                'rules' => 'required|valid_email|is_unique[User.email]',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                    'valid_email' => 'El %s tiene que contener una direccion valida', //manda error si el formato de email no es valido
                    'is_unique' => 'El %s ya está ocupado.' // manda error si el correo ya esta en alguno de los usuarios registrados
                )
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                )
            ),
            array(
                'field' => 'name',
                'label' => 'Nombre',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'El %s es requerido.', // manda error si no hay datos encontrados
                )
            ),
            array(
                'field' => 'lastname',
                'label' => 'Apellidos',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'Sus %s son requeridos.', // manda error si no hay datos encontrados
                )
            ),
            array(
                'field' => 'edad',
                'label' => 'Edad',
                'rules' => 'required|is_natural_no_zero|max_length[3]',
                'errors' => array(
                    'required' => 'La %s es requerida.', // manda error si no hay datos encontrados
                    'is_natural_no_zero' => 'La %s no es Valida.', // manda error si el dato no es un numero natural
                    'max_length' => 'La %s sobre pasa la cantidad de digitos permitidos'
                )
            ),
            array(
                'field' => 'dir',
                'label' => 'Dirección',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'La %s es requerida.' // manda error si el dato no es un numero natural
                )
            ),
            array(
                'field' => 'cp',
                'label' => 'CP',
                'rules' => 'required|is_natural_no_zero|max_length[5]',
                'errors' => array(
                    'required' => 'El %s es requerido.',
                    'is_natural_no_zero' => 'El %s no es Valido.', // manda error si el dato no es un numero natural
                    'max_length' => 'El %s sobre pasa la cantidad de digitos permitidos'
                )
            ),
            array(
                'field' => 'alcaldia',
                'label' => 'Alcaldia',
                'rules' => 'required',
                'errors' => array(
                    'required' => 'La %s es requerida.', // manda error si no hay datos encontrados
                )
            ),
        );
    }
}