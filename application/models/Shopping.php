<?php

class Shopping extends CI_Model{

	public function __construct()
	{
		//$this->load->database();
	}
	
	public function GetBooksInShoppingCart($iduser)
	{
		$this->db->select("*");
		$this->db->from("ShoppingCar");
		$this->db->where('IdUser',$iduser);
		$query = $this->db->get();
		return $query->result();
	}

	public function ExistBooksInShoppingCart($iduser,$idbook)
	{
		$this->db->select("*");
		$this->db->from("ShoppingCar");
		$this->db->where('IdUser',$iduser);
		$this->db->where('IdBook',$idbook);
		$query = $this->db->get();
		if($query->num_rows() != 0)
		{
			return true;
		}
		return false;
	}

	public function AddBooktoCar($datashoppingcar)
	{
		return !$this->db->insert('ShoppingCar',$datashoppingcar) ? false : true; 
	}

	public function deleteBookForShoppingCar($idbook)
	{
		return !$this->db->delete('ShoppingCar', array('IdBook' => $idbook)) ? false : true;
	}

	public function CountBooksInTheCar($iduser)
	{
		$this->db->select("*");
		$this->db->from("ShoppingCar");
		$this->db->where('IdUser',$iduser);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		return 0;
	}
	
	public function AddCard($card)
	{
		return !$this->db->insert('PayCard',$card) ? false : true; 
	}

	public function AddSold($sold)
	{
		return !$this->db->insert('Buy',$sold) ? false : true; 
	}

	public function CardForIdUserAndIdBook($iduser,$idbook)
	{
		$this->db->select("P.*,B.IdBook");
		$this->db->from("PayCard P,Buy B");
		$this->db->where('P.IdUser',$iduser);
		$this->db->where('B.IdBook',$idbook);
		$query = $this->db->get();
		return $query->row();
	}

	public function ExistCardInPayCard($iduser)
	{
		$this->db->select("*");
		$this->db->from("PayCard");
		$this->db->where('IdUser',$iduser);
		$query = $this->db->get();
		if($query->num_rows() != 0)
		{
			return true;
		}
		return false;
	}
}