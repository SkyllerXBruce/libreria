<?php 

class Category extends CI_Model{
	
	public function __construct()
	{
		$this->load->database();
  }
    
  public function GetCategory()
	{	// busca la tabla catalogue que contiene IdCategory,name
		$this->db->select("*");
    $this->db->from("Category");
		$query = $this->db->get();
    return $query->result();
	}

	public function FindCategoryForId($idcategory)
	{
		$this->db->select("*");
		$this->db->from("Category");
		$this->db->where('IdCategory',$idcategory);
		$query = $this->db->get();
    return $query->row();
	}

}