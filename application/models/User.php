<?php

class User extends CI_Model{

	public function __construct()
	{
		$this->load->database();
  }
  
  public function Access($email,$pass)
  {
    $data = $this->db->get_where('User',array('Email' => $email,'Password' => $pass));
    if($data->result()){
      return $data->row();
    }
    return false; 
  }

  public function register($userdata,$infouserdata)
  {
    $this->db->trans_start();
      $this->db->insert('User',$userdata); 
      $infouserdata['IdUser'] = $this->db->insert_id();   
      $this->db->insert('InfoUser',$infouserdata);
    $this->db->trans_complete(); // Termina la transaccion
    return !$this->db->trans_status() ? false : true; 
  }

  public function GetInfoUserForIdUser($iduser)
  {
    $this->db->select("*");
    $this->db->from("InfoUser");
    $this->db->where('IdUser',$iduser);
    $query = $this->db->get();
    return $query->row();
  }
}