<?php

class Books extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}

	public function GetBooks()
	{ //tabla Books con IdBook,Title,Author,Editorial,Year,Format,ISBN,Description,Price,DirImage,Category
		$this->db->select("*");
		$this->db->from("Book");
		$query = $this->db->get();
    return $query->result();
	}
	
	public function SuggestBooks()
	{
		$books = $this->GetBooks();
		$found = array();
		foreach ($books as $i => $book) {
			if ($i < 3) {
				array_push($found,$book);
			}
		}
		return $found;
	}

	public function AddBook($databook)
	{
		return !$this->db->insert('Book',$databook) ? false : true; 
	}

	public function FindBooksForSearch($searching)
	{
		$this->db->select("*");
		$this->db->from("Book");
		$this->db->like('Title', $searching); 
		$this->db->or_like('Author', $searching);
		$this->db->or_like('Editorial', $searching);
		$this->db->or_like('ISBN', $searching);
		$query = $this->db->get();
    return $query->result();	
	}

	public function FindCategoryForId($idcategory)
	{
		$this->db->select("*");
		$this->db->from("Category");
		$this->db->where('IdCategory',$idcategory);
		$query = $this->db->get();
    return $query->row();
	}

	public function FindBookForId($idbook)
	{
		$this->db->select("*");
		$this->db->from("Book");
		$this->db->where('IdBook',$idbook);
		$query = $this->db->get();
    return $query->result();
	}

	public function FindBookForIdCategory($idcategory)
	{
		$this->db->select("*");
		$this->db->from("Book B,Category C");
		$this->db->where('B.IdCategory = C.IdCategory');
		$this->db->where('B.IdCategory',$idcategory);
		$query = $this->db->get();
    return $query->result();
	}

	public function CountTotalBooks()
	{
		$this->db->select("*");
		$this->db->from("Book");
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->num_rows();
		}
		return 0;
	}
}

?>