<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library(array('form_validation','upload'));
    $this->load->helper(array('login_rules','users_rules','buying_rules','book_rules','string'));
    $this->load->model(array('Category','Books','User','Shopping'));
  }

  public function index()
  {
    $books = $this->Books->SuggestBooks();
    $vista = $this->load->view('Books/show_books',array('books' => $books),TRUE); 
    $this->getTemplate($vista);
  }

  public function category()
  {
    $data = $this->Category->GetCategory();
    $vista = $this->load->view('Books/category',array('category' => $data),TRUE);
    $this->getTemplate($vista);
  }

  public function login()
  {
    $vista = $this->load->view('Users/login','',TRUE);
    $this->getTemplate($vista);
  }

  public function AccessAccount()
  {
    $email = $this->input->post('email');
    $pass = $this->input->post('pass');
    $books = $this->Books->SuggestBooks();
    $this->form_validation->set_rules(getLoginRules());
    if ($this->form_validation->run() == FALSE) {
      $vista = $this->load->view('Users/login','',TRUE); 
    }else {
      if(!$res = $this->User->Access($email,$pass)){
        $vista = $this->load->view('Users/login',array(
          'error' => 'Las cedenciales no son correctas'),TRUE); 
      }else {
        $user = array(
          'iduser' => $res->IdUser,
          'name' => $res->Name,
          'email' => $res->Email,
          'type' => $res->Type,
          'is_logged' => true
        );
        $this->session->set_userdata($user);
        $vista = $this->load->view('Books/show_books',array(
          'books' => $books,'msg' => 'Bienvenido '.$res->Name),TRUE);
      }
    }
    $this->getTemplate($vista);
  }

  public function logout(){
    $numbooks = $this->Books->CountTotalBooks();
    $rndidbook = rand(1,$numbooks);
    $rndbook = $this->Books->FindBookForId($rndidbook);
    $vista = $this->load->view('Users/logout',array('book' => $rndbook),TRUE);
    $this->getTemplate($vista);
	}

  public function exit()
  {
    $vars = array('iduser','name','email','type','is_logged');
		$this->session->unset_userdata($vars);
		$this->session->sess_destroy();
		redirect('Home');
  }

  public function register()
  {
    $vista = $this->load->view('Users/register','',TRUE);
    $this->getTemplate($vista);
  }

  public function registerUser()
  {
    $user = $this->input->post('user');
    $correo = $this->input->post('correo');
    $pass = $this->input->post('password');
    $name = $this->input->post('name');
    $lastname = $this->input->post('lastname');
    $edad = $this->input->post('edad');
    $dir = $this->input->post('dir');
    $alcaldia = $this->input->post('alcaldia');
    $cp = $this->input->post('cp');
    $tel = $this->input->post('tel');   
    $this->form_validation->set_rules(getCreateUserRules());
    if ($this->form_validation->run() == FALSE) {
        $vista = $this->load->view('Users/register',array(
          'msg' => 'Faltan campos por completar'),TRUE);
    } else {
      $userdata = array(
        'Name' => $user,
        'Email' => $correo,
        'Password' => $pass,
        'Type' => 'User'
      );
      $infouserdata=array(
        'NameUser' => $name,
        'LastName' => $lastname,
        'Age' => $edad,
        'Dir' => $dir,
        'Alcaldia' => $alcaldia,
        'CP' => $cp,
        'PhoneNumber' => $tel,
        'PriceSend' => rand(180,450),
      );
      if (!$this->User->register($userdata,$infouserdata)) {
        $vista = $this->load->view('Users/register',array(
          'msg' => 'Ocurrió un error en la Base de datos, intente más tarde'),TRUE);
      } else {
        $books = $this->Books->SuggestBooks();
        $vista = $this->load->view('Books/show_books',array(
          'books' => $books,'msg' => 'El usuario '.$user.' quedó registrado'),TRUE);
      }
    }
    $this->getTemplate($vista);
  }

  public function contact()
  {
    $vista = $this->load->view('Users/contact','',TRUE);
    $this->getTemplate($vista);
  }

  public function search()
  {
    $search = $this->input->post('search');
    $foundbooks = $this->Books->FindBooksForSearch($search);
    $vista = $this->load->view('Books/search',array(
      'search' => $search,'foundbooks' => $foundbooks),TRUE);
    $this->getTemplate($vista);
  }

  public function searchCategory($idcategory)
  {
    $foundbooks = $this->Books->FindBookForIdCategory($idcategory);
    $datacategory = $this->Category->FindCategoryForId($idcategory);
    $category = $datacategory->Name;
    $vista = $this->load->view('Books/search',array(
      'search' => $category,'foundbooks' => $foundbooks),TRUE);
    $this->getTemplate($vista);
  }

  public function RegisterBook()
  {
    if (!$this->session->userdata('is_logged')) {
      $vista = $this->load->view('Users/login','',TRUE);
    } else {
      $category = $this->Category->GetCategory();
      $vista = $this->load->view('Books/register_books',array('category' => $category),TRUE);
    }
    $this->getTemplate($vista);
  }

  public function SaveBook()
  {
    $title = $this->input->post('title');
    $autor = $this->input->post('autor');
    $editorial = $this->input->post('editorial');
    $format = $this->input->post('format');
    $price = $this->input->post('price');
    $year = $this->input->post('year');
    $isbn = $this->input->post('isbn');
    $cat = $this->input->post('cat');
    $desc = $this->input->post('desc');  
    $this->form_validation->set_rules(getCreateBookRules());
    if ($this->form_validation->run() == FALSE) {
      $category = $this->Category->GetCategory();
      $vista = $this->load->view('Books/register_books',array(
        'category' => $category,'error' => 'Faltan Datos del Libro'),TRUE);
    } else {
      $databook = array(
        'Title' => $title, 
        'Author' => $autor, 
        'Editorial' => $editorial, 
        'Format' => $format, 
        'Price' => $price, 
        'Year' => $year, 
        'ISBN' => $isbn, 
        'IdCategory' => $cat, 
        'Description' => $desc, 
        'DirImage' => 'assets/images/No_picture_available.png', 
      );
      if (!$this->Books->AddBook($databook)) {
        $category = $this->Category->GetCategory();
        $vista = $this->load->view('Books/register_books',array(
          'category' => $category,'error' => 'No se pudo añadir el libro, intente más tarde'),TRUE);
      } else {
        $category = $this->Category->GetCategory();
        $vista = $this->load->view('Books/register_books',array(
          'category' => $category,'msg' => 'Libro Agregado'),TRUE);
      }
      
    }
    $this->getTemplate($vista); 
  }

  public function AddBooktoCar()
  {
    if (!$this->session->userdata('is_logged')) {
      $vista = $this->load->view('Users/login','',TRUE);
    } else { 
      $iduser = $this->session->userdata('iduser');
      $idbook = $this->input->post('IdBook');
      $search = $this->input->post('search');
      if ($this->Shopping->ExistBooksInShoppingCart($iduser,$idbook)) {
        $error='El libro ya esta en el carrito';
        $vista = $this->ErrorView($error,$search);
      } else {
        if ($search==null) {
          $book = $this->Books->FindBookForId($idbook);
          $idcategory = $book[0]->IdCategory;
          $foundcategory = $this->Category->FindCategoryForId($idcategory);
          $search = $foundcategory->Name;
        } 
        if (empty($idbook)) {
          $error ='Hubo un problema, intente más tarde';
          $vista = $this->ErrorView($error,$search);
        } else {
          $iduser = $this->session->userdata('iduser');
          $datashoppingcar = array(
            'IdBook' => $idbook, 
            'IdUser' => $iduser,
            'SearchOrCategory' => $search,
          );
          if (!$this->Shopping->AddBooktoCar($datashoppingcar)) {
            $error='Hubo un problema para añadir el libro al carrito, intente más tarde';
            $vista = $this->ErrorView($error,$search);
          } else {
            $msg='Libro Añadido';
            $books = $this->Books->SuggestBooks();
            $vista = $this->load->view('Books/show_books',array(
              'books' => $books, 'msg' => $msg),TRUE); 
          }
        }
      }
    }
    $this->getTemplate($vista);
  }

  public function ErrorView($error,$searching)
  {
    $foundbooks = $this->Books->FindBooksForSearch($searching);
    $vista = $this->load->view('Books/search',array(
      'search' => $searching,'foundbooks' => $foundbooks,'error' => $error),TRUE);
    return $vista;
  }
  
  // Método Template que Carga todos los elemento de las Vistas
  public function getTemplate($view)
  {
    $title = 'Home'; // titulo del Encabezado
    $name='Ingresa';
    $is_online=false;
    $data = $this->Category->GetCategory();
    $numbooks = 0;
    $is_admin = false;
    if ($this->session->userdata('is_logged')) {
      $name = $this->session->userdata('name');
      $is_online=true;
      $numbooks = $this->Shopping->CountBooksInTheCar($this->session->userdata('iduser'));
      if ($this->session->userdata('type') =='Admin') {
        $is_admin = true;
      }
    }
    // Partes de la vista 
    $allviews = array(
      'head' => $this->load->view('Layout/head',array('title' => $title),TRUE), // Encabezado
      'nav' => $this->load->view('Layout/nav',array(
        'category' => $data,'numbooks' => $numbooks,'name' => $name,'is_online' => $is_online
      ),TRUE),
      'content' => $view, // Contenido de la pagina
      'footer' => $this->load->view('Layout/footer',array(
        'is_online' => $is_online,'is_admin' => $is_admin),TRUE) // Pie de pagina
    );
    $this->load->view('home',$allviews); // mandamos todo al home
  }
}
